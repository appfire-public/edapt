/*******************************************************************************
 * Copyright (c) 2007, 2010 BMW Car IT, Technische Universitaet Muenchen, and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BMW Car IT - Initial API and implementation
 *     Technische Universitaet Muenchen - Major refactoring and extension
 *******************************************************************************/
package org.eclipse.emf.edapt.migration.execution;

import java.io.IOException;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.edapt.common.IResourceSetFactory;
import org.eclipse.emf.edapt.history.Release;
import org.eclipse.emf.edapt.migration.Metamodel;
import org.eclipse.emf.edapt.migration.Model;
import org.eclipse.emf.edapt.migration.Persistency;

public class MigrationReconstructor extends AbstractMigrationReconstructor {

	private final List<URI> modelURIs;

	/** Constructor. */
	public MigrationReconstructor(List<URI> modelURIs, Release sourceRelease, Release targetRelease, IClassLoader classLoader,
			ValidationLevel level, IResourceSetFactory resourceSetFactory) {
		super(sourceRelease, targetRelease, classLoader, level, resourceSetFactory);
		this.modelURIs = modelURIs;
	}

	@Override
	protected Model loadModel(Metamodel metamodel) throws IOException {
		return Persistency.loadModel(modelURIs, metamodel, resourceSetFactory);
	}
}
