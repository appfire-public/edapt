/*******************************************************************************
 * Copyright (c) 2007, 2010 BMW Car IT, Technische Universitaet Muenchen, and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BMW Car IT - Initial API and implementation
 *     Technische Universitaet Muenchen - Major refactoring and extension
 *******************************************************************************/
package org.eclipse.emf.edapt.migration.execution;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.edapt.common.URIUtils;
import org.eclipse.emf.edapt.migration.MigrationException;

/**
 * Registry for all migrators (singleton). A migrator is registered as an
 * Eclipse extension.
 * 
 * @author herrmama
 * @author $Author$
 * @version $Rev$
 * @levd.rating RED Rev:
 */
public final class MigratorRegistry {

	/** Singleton instance. */
	private static MigratorRegistry migratorRegistry;

	/** Registered migrators identified by unversioned namespace URI. */
	private final Map<String, Migrator> migrators;

	/** Private default constructor. */
	private MigratorRegistry() {
		migrators = new HashMap<String, Migrator>();
	}

	/** Getter for instance. */
	public static MigratorRegistry getInstance() {
		if (migratorRegistry == null) {
			migratorRegistry = new MigratorRegistry();
		}
		return migratorRegistry;
	}

	/** Register a migrator by its URL. */
	public void registerMigrator(URL migratorURL, IClassLoader loader) throws MigrationException {
		Migrator migrator = new Migrator(URIUtils.getURI(migratorURL), loader);
		for (String nsURI : migrator.getNsURIs()) {
			migrators.put(nsURI, migrator);
		}
	}

	/** Register a migrator by its URI. */
	public void registerMigrator(URI migratorURI, IClassLoader loader) throws MigrationException {
		registerMigrator(URIUtils.getURL(migratorURI), loader);
	}

	/** Get a migrator by its namespace already stripped from version. */
	public Migrator getMigrator(String nsURI) {
		return migrators.get(nsURI);
	}

}
