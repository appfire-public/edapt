package org.eclipse.emf.edapt.migration.execution;

import java.io.IOException;
import java.io.InputStream;

import org.eclipse.emf.edapt.common.IResourceSetFactory;
import org.eclipse.emf.edapt.history.Release;
import org.eclipse.emf.edapt.migration.Metamodel;
import org.eclipse.emf.edapt.migration.Model;
import org.eclipse.emf.edapt.migration.Persistency;

public class StreamingMigrationReconstructor extends AbstractMigrationReconstructor {

	private final InputStream modelStream;

	public StreamingMigrationReconstructor(InputStream modelStream, Release sourceRelease, Release targetRelease, IClassLoader classLoader,
			ValidationLevel level, IResourceSetFactory resourceSetFactory) {
		super(sourceRelease, targetRelease, classLoader, level, resourceSetFactory);
		this.modelStream = modelStream;
	}

	@Override
	protected Model loadModel(Metamodel metamodel) throws IOException {
		return Persistency.loadModel(modelStream, metamodel, resourceSetFactory);
	}

}
